﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Pulses a gameObjects local scale if "Pulsing" is activated. Good for simple animations.
/// </summary>
public class PulseObjectBehavior : MonoBehaviour {

	public bool Pulsing;

	public float MaxPulse = 1;
	public float MinPulse = 0;
	public float PulseSpeed = 1;

	private Vector3 OriginalScale;	
	private Vector3 MaxPulseVector;	
	private Vector3 MinPulseVector;


	// Use this for initialization
	void Start () {

		OriginalScale = transform.localScale;
		MaxPulseVector = OriginalScale + ( OriginalScale * MaxPulse );
		MinPulseVector = OriginalScale + ( OriginalScale * MinPulse );

	}

    public void StartAnimation() {
        Pulsing = true;
    }

	bool PulseUp;

	// Update is called once per frame
	void Update () {

		if(!Pulsing) return;

		if(PulseUp){
			transform.localScale = Vector3.MoveTowards(transform.localScale, MaxPulseVector, Time.deltaTime*PulseSpeed);
			if(transform.localScale==MaxPulseVector) PulseUp = false;
		}else{
			transform.localScale = Vector3.MoveTowards(transform.localScale, MinPulseVector, Time.deltaTime*PulseSpeed);
			if(transform.localScale==MinPulseVector) PulseUp = true;
		}
	
	}
}
