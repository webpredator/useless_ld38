﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStateController : MonoBehaviour {

    public TextMeshProUGUI RemainingTime;
    public float GameTimeSeconds = 600;
    public float GameTimeDisplay = 46; // billion light years
    public string GameTimeTemplate = "{0.0} billion light-years";

    float _gameOverTime;
    private PulseObjectBehavior _textPulse;

    void Start () {

        _gameOverTime = Time.time + GameTimeSeconds;
        _textPulse = RemainingTime.GetComponent<PulseObjectBehavior>();

        Invoke("GameOver", GameTimeSeconds);
    }
	
	
	void Update () {

        var remainingTime = _gameOverTime - Time.time;
        //percentage of time left
        var lerp = Mathf.InverseLerp(0, GameTimeSeconds, remainingTime);
        var ly = Mathf.Lerp(0, GameTimeDisplay, lerp);

       // RemainingTime.text = string.Format(GameTimeTemplate, remainingTime);
        RemainingTime.text = string.Format(GameTimeTemplate, ly);

        if (lerp < 0.2f && !_textPulse.Pulsing) _textPulse.Pulsing = true;

        

    }

    void GameOver() {

        if (GameStatus.Instance != null)
        {
            GameStatus.Instance.Plays++;
            GameStatus.Instance.LastPrayers = (int)ResourceController.Instance.Prayers;
        }
        SceneManager.LoadScene("intro");

    }

}
