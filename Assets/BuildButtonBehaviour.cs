﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BuildButtonBehaviour : MonoBehaviour {

    public TextMeshProUGUI Text;

    [Multiline]
    public string Template = "{0} {1}w {2}g";
    public Image Icon;
    bool _init = false;
    private BuildingConfig _config;
    Button _btn;

    void Awake () {
        _btn = GetComponent<Button>();	
	}
	
	void Update () {

        if (!_init) return;

        _btn.interactable = ResourceController.Instance.CanBuild(_config);

	}

    internal void SetConfig(BuildingConfig b)
    {
        _config = b;
        Text.text = string.Format(Template, b.Title, b.CostWood, b.CostGold);
        Icon.sprite = b.Sprite;
        Icon.color = b.Color;
        _init = true;
        _btn.onClick.AddListener(() => OnClick());
    }

    void OnClick()
    {
        if (!ResourceController.Instance.CanBuild(_config)) return;
        ResourceController.Instance.PayFor(_config);
        BuildMenuController.Instance.Build(_config);    
    }
}
