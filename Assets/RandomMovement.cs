﻿using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;
using DG.Tweening;

public class RandomMovement : MonoBehaviour {

	public Collider2D MovementBounds;

    public float MaxJumpDistance = 0.05f;
    public Vector2 JumpDuration = new Vector2(0.3f, 1);
    public Vector2 JumpPower = new Vector2(0.005f,0.03f);
    public Vector2 NumJumps = new Vector2(1, 4);
    public Vector2 NextJumpTime = new Vector2(1, 3);

    void Start(){
        Invoke("Jump", 0);
	}


    void Jump()
    {

        Vector3 randomDirection;
        int iterations = 0;
        do
        {
            // find next pos around me but make sure it is inside the boundaries
            randomDirection = transform.position + (Random.insideUnitSphere * MaxJumpDistance);
            randomDirection.z = 0;
        //    Debug.Log(randomDirection);
            iterations++;
            if (iterations > 100) break;
        } while (!IsPosValid(randomDirection));
        //  Debug.Log("took "+iterations+ " move"+randomDirection);

        var duration = Random.Range(JumpDuration.x, JumpDuration.y);
        transform.DOJump(randomDirection, Random.Range(JumpPower.x, JumpPower.y), Random.Range((int)NumJumps.x, (int)NumJumps.y), duration);

        Invoke("Jump", duration+Random.Range(NextJumpTime.x, NextJumpTime.y));

    }


    bool IsPosValid(Vector3 pos)
    {
        return MovementBounds.OverlapPoint(pos);
    }


}
