﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResourceController : MonoBehaviour {

    public TextMeshProUGUI ResourcesText;
    public TextMeshProUGUI PrayersText;
    public TextMeshProUGUI PopulationText;

    [Multiline]
    public string Template = "{0}g {1}w";
    public string TemplatePrayers = "Prayers: {0}";
    public string TemplatePopulation = "Population: {0}";

    public float Wood;
    public float Gold;
    public float Prayers;
    public float People;

    public int DefaultWood = 1;
    public int DefaultGold = 1;

    public static ResourceController Instance { get; private set; }
    void Awake()
    {
        Instance = this;

        InvokeRepeating("AddDefaultResources", 1, 1);

    }

    public bool CanBuild(BuildingConfig building) {
        return building.CostWood <= Wood && building.CostGold <= Gold;
    }

    void AddDefaultResources() {
        Wood += DefaultWood;
        Gold += DefaultGold;
    }
    
	
	void Start () {
		
	}
	
	
	void Update () {
        ResourcesText.text = string.Format(Template, Wood, Gold);
        PrayersText.text = string.Format(TemplatePrayers, Prayers);
        PopulationText.text = string.Format(TemplatePopulation, People);
    }

    internal void PayFor(BuildingConfig _config)
    {
        Wood -= _config.CostWood;
        Gold -= _config.CostGold;
        Events.Post(this, Assets.EventEnum.PaidSomething);
    }
}
