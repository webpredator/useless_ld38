﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetClickHandler : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    public LayerMask HitLayer;
    public float HitRadius;

	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonUp(0))
        {
            var worldClick = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var worldClick2d = new Vector2(worldClick.x, worldClick.y);
            worldClick2d.y -= 0.01f;
            //Debug.Log(worldClick);

            var raycastHit = Physics2D.CircleCast(worldClick2d, HitRadius, Vector2.up, 1, HitLayer);

            if (raycastHit.collider != null) {
                var b = raycastHit.collider.GetComponent<BuildingBehaviour>();
                b.Harvest();
                //Debug.Log(raycastHit.collider.name);
            }
            
        }
        	
	}
    

}
