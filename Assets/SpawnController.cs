﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour {

    public GameObject PersonPrefab;
    
    public Collider2D PersonMovementBounds;
    public BuildMenuController BuildMenu;

    public GameObject UiMsgPrefab;
    public Canvas Canvas;

    public static SpawnController Instance { get; private set; }
    void Awake()
    {
        Instance = this;
    }

    
    public void SpawnPerson(Vector3 pos, int amount)
    {

        for (int i = 0; i < amount; i++)
        {
            var go = (GameObject)Instantiate(PersonPrefab, pos, Quaternion.identity);
            go.transform.SetParent(PersonMovementBounds.transform);
            go.GetComponent<RandomMovement>().MovementBounds = PersonMovementBounds;
            
        }

    }

    public void SpawnUiMsg(Vector3 pos, string msg, Color color)
    {
        
        var go = (GameObject)Instantiate(UiMsgPrefab, pos, Quaternion.identity);
        go.transform.SetParent(Canvas.transform, true);
        go.transform.localScale = Vector3.one;
        go.GetComponent<UiMsgBehaviour>().Init(msg, color);

    }

}
