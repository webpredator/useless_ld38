﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatus : MonoBehaviour {

    public int Plays;
    public int LastPrayers;

    public static GameStatus Instance { get; private set; }
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }else
        {
            Destroy(gameObject);
        }

        //Plays = 1;


    }

    void Start () {
        DontDestroyOnLoad(gameObject);
	}
	
}
