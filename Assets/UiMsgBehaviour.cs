﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class UiMsgBehaviour : MonoBehaviour {

    public float Ttl = 3;
    public Vector3 MoveAmount = new Vector3(0,1,0);
    public float MoveSpeed = 1;


    private void Start()
    {
      //  Init("hello", Color.yellow);
    }

	public void Init (string msg, Color color) {

        transform.DOMove(transform.position + MoveAmount, MoveSpeed);
        var txtMesh = GetComponent<TextMeshProUGUI>();
        txtMesh.text = msg;
        txtMesh.color = color;
        txtMesh.DOFade(0, MoveSpeed);

        Invoke("DestroyDelayed", Ttl);
	}
	

	
	void DestroyDelayed() {
        Destroy(gameObject);	
	}
}
