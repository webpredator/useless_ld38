﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroController : MonoBehaviour {

    public Animator CamAnim;
    public TextMeshProUGUI LastResult;
    public GameObject PlanetStart;

    private void Start()
    {
        /*
        GameStatus.Instance.Plays = 1;
        GameStatus.Instance.LastPrayers = 555;
        */
        LastResult.text = GameStatus.Instance.LastPrayers + "p";

        if (GameStatus.Instance.Plays > 0)
        {
            Events.Post(this, Assets.EventEnum.OutroStart);
        }else
        {
            Events.Post(this, Assets.EventEnum.GameStart);
        }

        Events.AddListener(this, Assets.EventEnum.IntroComplete, OnIntroComplete());
    }

    
    public void StartGame() {

        Events.Post(this, Assets.EventEnum.IntroStart);
      //  StartCoroutine(CamMovement());

    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void About()
    {
        Application.OpenURL("http://42bytes.rocks/");
    }

    private IEnumerator OnIntroComplete()
    {
        CamAnim.SetTrigger("zoom");
        yield return new WaitForSeconds(1);
        PlanetStart.SetActive(true);
        yield return new WaitForSeconds(2.5f);
        SceneManager.LoadScene("game");
    }
}
