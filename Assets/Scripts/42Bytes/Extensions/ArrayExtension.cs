﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets._42Bytes
{
    public static class ArrayExtension
    {
        public static T PickRandom<T>(this T[] source)
        {
            var randomIndex = Random.Range(0, source.Length);
        //    Debug.Log("picking "+randomIndex+" fron "+source.Length);
            return source[randomIndex];
            //return source.PickRandom(1).Single();
        }

        public static T PickRandom<T>(this List<T> source)
        {
            var randomIndex = Random.Range(0, source.Count);
            return source[randomIndex];
            //return source.PickRandom(1).Single();
        }
        
        public static void Shuffle<T>(this IList<T> list)
        {
            var r = new System.Random();
            
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = r.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static T PickRandom<T>(this IEnumerable<T> source)
        {
            var randomIndex = Random.Range(0, source.Count());
            return source.ElementAt(randomIndex);
        }

        /*
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
        {
            return source.OrderBy(x => Guid.NewGuid());
        }
        */

    }

}
