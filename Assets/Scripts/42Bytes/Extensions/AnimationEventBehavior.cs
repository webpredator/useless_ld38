﻿using UnityEngine;
using System.Collections;

namespace Assets._42Bytes
{
    /// <summary>
    /// Lists some generic methods that the animator can assign and other scripts can subscribe to the events
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class AnimationEventBehavior : MonoBehaviour
    {

        public delegate void AnimationEvent();
        public event AnimationEvent OnAnimationStarting;
        public event AnimationEvent OnAnimationEnding;


        protected void AnimationStarting()
        {
            if (OnAnimationStarting != null)
            {
                OnAnimationStarting();
            }
        }

        protected void AnimationEnding()
        {
            if (OnAnimationEnding != null)
            {
                OnAnimationEnding();
            }
        }


    }

}