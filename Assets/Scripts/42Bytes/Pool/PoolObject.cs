﻿using Assets._42BYTES.Pool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets._42Bytes.Pool
{
    public class PoolObject : MonoBehaviour
    {

        /// <summary>
        /// Associated pool of this object
        /// </summary>
        public AbstractPool Pool;

        /// <summary>
        /// Should this object be destroyed rather than deactivated?
        /// </summary>
        public bool DestroyOnDespawn = false;

        /// <summary>
        /// Despawn this element into the correct pool
        /// </summary>
        public void Despawn()
        {
            if (DestroyOnDespawn)
            {
                Destroy(gameObject);
            }
            else
            {
                Pool.Despawn(gameObject);
            }
        }

    }
}
