﻿using Assets._42Bytes.Pool;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets._42BYTES.Pool
{
    /// <summary>
    /// Contains a list of random game objects that can be spawned and despawned.
    /// All objects are based on the list of prefabs. 
    /// - Prewarm(123) creates a list of 123 objects upfront but deactivated.
    /// - With Spawn it will take the next object from the list. If there is nothing 
    /// left in the pool another one will be created. 
    /// - Despawn will add it back to the list.
    /// </summary>
    public abstract class AbstractPool : MonoBehaviour
    {
        private List<GameObject> _clones = new List<GameObject>();

        protected abstract GameObject GetPrefab();

        public bool DestroyOnDespawn = false;

        /// <summary>
        /// Creates a list of objects in the pool
        /// </summary>
        /// <param name="amount"></param>
        public void Prewarm(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                Prewarm();
            }
        }

        /// <summary>
        /// Creates and returns a single object in the pool
        /// </summary>
        /// <returns></returns>
        public GameObject Prewarm()
        {
            var prefab = GetPrefab();
            var clone = (GameObject)Instantiate(prefab, Vector3.zero, prefab.transform.rotation);
            var poolObject = clone.AddComponent<PoolObject>();
            poolObject.Pool = this;
            poolObject.DestroyOnDespawn = DestroyOnDespawn;
            clone.SetActive(false);
            clone.transform.SetParent(transform);
            _clones.Add(clone);
            //Debug.Log("added "+clone.name+" to the pool");
            return clone;
        }

        /// <summary>
        /// Removes and returns the last clone from the list.
        /// Will NOT create a new clone if pool is empty
        /// </summary>
        /// <returns></returns>
        public GameObject TakeFirstClone()
        {
            if (_clones.Count < 1) return null;
            var clone = _clones[0];
            _clones.RemoveAt(0);
            return clone;
        }

        /// <summary>
        /// Will activate a clone at the supplied position
        /// </summary>
        /// <param name="position"></param>
        public GameObject Spawn(Vector3 position, Quaternion rotation)
        {
            var clone = TakeFirstClone();
            
            // out of clones? create one first
            if (clone == null)
            {
                Prewarm();
                clone = TakeFirstClone();
            }

            clone.transform.SetParent(null);
            clone.transform.position = position;
            clone.transform.rotation = rotation;
            clone.SetActive(true);

            //Debug.Log("spawn object at "+position);

            return clone;
        }

        public GameObject Spawn(Vector3 position)
        {
            return Spawn(position, Quaternion.identity);
        }

        /// <summary>
        /// Activates a clone and returns a specific component (easier access)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="position"></param>
        /// <returns></returns>
        public T Spawn<T>(Vector3 position)
        {
            var obj = Spawn(position);
            return obj.GetComponent<T>();
        }

        public T Spawn<T>(Vector3 position, Quaternion rotation)
        {
            var obj = Spawn(position, rotation);
            return obj.GetComponent<T>();
        }

        public void Despawn(GameObject obj)
        {
            // inject at end
            _clones.Add(obj);
            obj.SetActive(false);
            obj.transform.SetParent(transform);
        }
    }
}
