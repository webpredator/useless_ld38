﻿using Assets._42Bytes;
using Assets._42Bytes.Pool;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets._42BYTES.Pool
{
    /// <summary>
    /// Contains a list of random game objects that can be spawned and despawned.
    /// All objects are based on the list of prefabs. 
    /// - Prewarm(123) creates a list of 123 objects upfront but deactivated.
    /// - With Spawn it will take the next object from the list. If there is nothing 
    /// left in the pool another one will be created. 
    /// - Despawn will add it back to the list.
    /// </summary>
    public class RandomPool : AbstractPool
    {
        public GameObject[] Prefabs;

        protected override GameObject GetPrefab() {
            return Prefabs.PickRandom();
        }
        
    }
}
