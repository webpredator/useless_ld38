﻿using Assets._42Bytes.Interfaces;
using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets._42Bytes.Effects
{
    public class MoveInEffect : MonoBehaviour, IAnimatable
    {

        public Vector3 MoveOutOfSight = new Vector3(0, 10);


        public Ease Easing;
        public float Speed;
        public float Period;
        public float Amplitude;
        public bool Reset;

        Vector2 _targetPos;


        void Start() {
        }

        public void SetTargetPos(Vector3 targetPos)
        {
            _targetPos = targetPos;
        }

        public void StartAnimation()
        {
            _targetPos = transform.position;
            transform.Translate(MoveOutOfSight);

            transform
                .DOMove(_targetPos, Speed)
                .SetEase(Easing, Amplitude, Period)
                .Play();
        }

        void Update() {
            if (Reset)
            {
                Reset = false;
                transform.Translate(MoveOutOfSight);
                StartAnimation();
            }
        }

    }
}
