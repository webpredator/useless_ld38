﻿/*
	FadeObjectInOut.cs
 	Hayden Scott-Baron (Dock) - http://starfruitgames.com
 	6 Dec 2012 
 
	This allows you to easily fade an object and its children. 
	If an object is already partially faded it will continue from there. 
	If you choose a different speed, it will use the new speed. 
 
	NOTE: Requires materials with a shader that allows transparency through color.  
*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Assets._42Bytes.Interfaces;

public class FadeInOutOpacity : MonoBehaviour, IAnimatable
{
    enum FadeDirection
    {
        None,
        In,
        Out
    }

	public void StartAnimation(){
		//Debug.Log("fading in");
		FadeIn();
	}
    
    public float fadeSpeed = 0.5f;


    private FadeDirection CurrentDirection;
    private float fadeTime = 0;


    public void FadeIn()
    {
        fadeTime = 0;
        CurrentDirection = FadeDirection.In;
    }

    public void FadeOut()
    {
        fadeTime = 0;
        CurrentDirection = FadeDirection.Out;
    }

    public void Reset() {
        if(isImg)
        {
            Img.color = EndColor;
        }
        else
        {
            Sprite.color = EndColor;
        }
        
    }

    private Image Img;
    private SpriteRenderer Sprite;

    private Color StartColor;
    private Color EndColor;

    private bool isImg = true;

    void Awake() {
        Img = GetComponent<Image>();
        if (Img == null)
        {
            isImg = false;
            Sprite = GetComponent<SpriteRenderer>();
        }
        StartColor = isImg ? Img.color : Sprite.color;
        var c = isImg ? Img.color : Sprite.color;
        c.a = 0;
        EndColor = c;
    }

    void Start() {
        
        
    }


    void Update()
    {
        if (CurrentDirection == FadeDirection.In)
        {
            //Img.color = Color.Lerp(StartColor, EndColor, fadeTime * Time.deltaTime);
            var lerp = Mathf.Lerp(0, 1, fadeTime * fadeSpeed);
            fadeTime += Time.deltaTime;
            LerpAlpha(lerp);
            

            //Img.color = Color.Lerp(Img.color, Color.clear, fadeTime * Time.deltaTime);
        }
        if (CurrentDirection == FadeDirection.Out)
        {
            var lerp = Mathf.Lerp(1, 0, fadeTime * fadeSpeed);
            fadeTime += Time.deltaTime;
            LerpAlpha(lerp);
            //Img.color = Color.Lerp(EndColor, StartColor, fadeTime * Time.deltaTime);
            //Img.color = Color.Lerp(Color.clear, Img.color, fadeTime * Time.deltaTime);
        }

    }

    void LerpAlpha(float a)
    {
        var c = isImg? Img.color:Sprite.color;
        c.a = a;
        if (isImg)
        {
            Img.color = c;
        }else
        {
            Sprite.color = c;
        }
    }


}