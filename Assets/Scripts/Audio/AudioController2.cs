﻿using Assets._42BYTES.Pool;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    public class AudioController2 : MonoBehaviour
    {

        public static AudioController2 Instance;

        //public GameObject SoundEffectPrefab;
        public AudioClipConfig[] ClipConfigs;

        public StandardPool PrefabPool;
        public int PoolPrewarm = 10;

        void Awake()
        {
            Instance = this;

            PrefabPool.Prewarm(PoolPrewarm);

            /*
            Events.AddListener(this, EventEnum.Pickup, PlayPickup);
            Events.AddListener(this, EventEnum.PickupItem, PlayCoin);
            Events.AddListener(this, EventEnum.PickupInvincible, PlayInvincible);
            */
            //Events.AddListener(this, EventEnum.PartAdded, );

            
        }

        void Start() {

            foreach (var config in ClipConfigs)
            {
                /*
                if (!GameConstants.Build.IsRelease) {
                    config.Name = System.Enum.GetName(typeof(EventEnum), config.Event) + " (" + config.Clips.Length+ " clips)";
                }
                */
                Events.AddListener(this, config.Event, config.Play);
            }

        }

        public AudioClipPoolObject GetNewAudioSource() {
            return PrefabPool.Spawn<AudioClipPoolObject>(Vector3.zero);
            //var o = (GameObject)Instantiate(SoundEffectPrefab);
            //return o.GetComponent<AudioSource>();
        }

    }

    [Serializable]
    public class AudioClipConfig
    {

        //public string Name { get { return "TEST"; } }
        // just to be easier to list, no function
        public string Name;

        public EventEnum Event;
        public AudioClip[] Clips;


        public bool RandomizePitch;
        public Vector2 PitchRange = new Vector3(1,1);
        

        [Tooltip("Each call will increase the clip index instead of randomizing it")]
        public bool IncreaseClipIndexPerCall = false;

        [Tooltip("Play all clips instead of just a random one by default")]
        public bool PlayAll = false;

        [Tooltip("Distance between clips when all are played")]
        public float PlayAllDistance = 0.1f;

        public float Volume = 1;

        public float Delay = 0;

        public bool Loop = false;


        public bool DespawnOnEvent = false;
        public EventEnum DespawnEvent;


        int _clipIndex = -1;

        /// <summary>
        /// gets called by the event
        /// </summary>
        /// <param name="options"></param>
        public void Play(object options)
        {


            if (!PlayAll)
            {
                _submitPlay(Delay);
                return;
            }

            // play all in a sequence
            IncreaseClipIndexPerCall = true;
            for (int i = 0; i < Clips.Length; i++)
            {
                _submitPlay(i * PlayAllDistance);
            }

        }

        void _submitPlay(float delay=0) {

            if (IncreaseClipIndexPerCall)
            {
                _clipIndex++;
                if (_clipIndex >= Clips.Length)
                {
                    _clipIndex = 0;
                }
            }

            var audioSource = AudioController2.Instance.GetNewAudioSource();
            audioSource.Play(this, _clipIndex, delay);

            /*
            if (!GameConstants.Build.IsRelease)
            {
                if (Event == EventEnum.StartClicked) audioSource.Log();
            }
            */

        }



    }

}
