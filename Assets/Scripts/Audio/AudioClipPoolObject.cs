﻿using UnityEngine;
using System.Collections;
using Assets._42Bytes.Pool;
using Assets.Scripts.Controllers;
using Assets._42Bytes;

using Assets;
using DG.Tweening;

[RequireComponent(typeof(AudioSource))]
public class AudioClipPoolObject : MonoBehaviour {

    private AudioSource _source;
    private PoolObject _pool;

    void Awake() {
        _source = GetComponent<AudioSource>();
	}

    public void Play(AudioClipConfig config, int clipIndex, float delay)
    {

        StartCoroutine(_play(config, clipIndex, delay));

    }

    public void Log() {
        Invoke("_log", 1);
    }

    void _log() {
         //   NPBinding.UI.ShowToast(_source.clip.name, VoxelBusters.NativePlugins.eToastMessageLength.SHORT);
    }

    IEnumerator _play(AudioClipConfig config, int clipIndex, float delay)
    {

        yield return new WaitForSeconds(delay);

        var invalid = false;

        // pick clip
        if (clipIndex > -1 && clipIndex < config.Clips.Length)
        {
            _source.clip = config.Clips[clipIndex];
            if (_source.clip == null)
            {
                invalid = true;
            }
        }
        else
        {
            _source.clip = config.Clips.Length == 1 ? config.Clips[0] : config.Clips.PickRandom();
        }

        

        if (invalid)
        {
            Invoke("Despawn", 0.01f);
        }
        else
        {

//            Debug.Log("playing " + _source.clip.name);

            // pitch
            if (config.RandomizePitch)
            {
                _source.pitch = Random.Range(config.PitchRange.x, config.PitchRange.y);
            }else
            {
                _source.pitch = 1;
            }

            _source.loop = config.Loop;
            _source.volume = config.Volume;
            // start and destroy
            if (!config.Loop)
            {
                // with changing pitch we also change the length which is not reflected here so we have to despawn later
                var despawnTime = _source.clip.length / Mathf.Abs(_source.pitch);              
                Invoke("Despawn", despawnTime+ 0.5f);

            }
            _source.Play();

            // maybe despawn on certain event (to stop music etc)
            if (config.DespawnOnEvent)
            {
                //_source.GetComponent<DespawnOnEventBehaviour>().SetEvent(config.DespawnEvent);
                SetEvent(config.DespawnEvent);
            }
            

        }




    }

    void Despawn() {
        if(_pool==null)_pool = GetComponent<PoolObject>();
        _pool.Despawn();
    }


    EventEnum _event;

    public void SetEvent(EventEnum despawnEvent)
    {
        _event = despawnEvent;
        Events.AddListener(this, despawnEvent, DespawnFromEvent);
    }

    private void DespawnFromEvent(object obj)
    {
        // this causes iteration modified issues
     //   Events.RemoveListener(this, _event);

        _source.DOFade(0, 0.5f).Play();
        //GetComponent<PoolObject>().Despawn();
        Invoke("Despawn", 0.6f);
        //Despawn();
    }


}

