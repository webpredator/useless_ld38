﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public enum EventEnum
    {
        GameStart,
        GameOver,

        GotWood,
        GotGold,
        BuiltBuilding,
        PersonCreated,
        PrayerSent,

        PaidSomething,

        
        OutroComplete,
        OutroStart,
        IntroStart,
        IntroComplete,

        Explosion,
        Talking,
        Laughing,

        ButtonTap

    }
    

}
