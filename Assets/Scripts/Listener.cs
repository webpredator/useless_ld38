﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class Listener
    {
        public Component Component;

        // type 0
        public Action<object> Action;

        // type 1
        public Action ActionNoParams;

        // type 2
        public IEnumerator ActionEnum;

        public int ActionType { get; private set; }

        public Listener(Component sender, Action<object> action)
        {
            Component = sender;
            Action = action;
            ActionType = 0;
        }

        public Listener(Component sender, Action action)
        {
            Component = sender;
            ActionNoParams = action;
            ActionType = 1;
        }

        public Listener(Component sender, IEnumerator action)
        {
            Component = sender;
            ActionEnum = action;
            ActionType = 2;
        }



    }
}
