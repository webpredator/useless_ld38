﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets;
using System;


public class Events : MonoBehaviour
{

    const bool enableLog = true;

    public static Events Instance;

    void Awake() {
        Instance = this;
    }

    //Private variables
    //------------------------------------------------
    //Internal reference to all listeners for notifications
    private Dictionary<EventEnum, List<Listener>> Listeners = new Dictionary<EventEnum, List<Listener>>();

    //Methods
    //------------------------------------------------
    //Function to add a listener for an notification to the listeners list
    public static void AddListener(Component Sender, EventEnum NotificationName, Action<object> listenerAction)
    {
        //Add listener to dictionary

        if (!Instance.Listeners.ContainsKey(NotificationName))
            Instance.Listeners.Add(NotificationName, new List<Listener>());

        //Add object to listener list for this notification
        Instance.Listeners[NotificationName].Add(new Listener(Sender, listenerAction ));
    }

    public static void AddListener(Component Sender, EventEnum NotificationName, Action listenerAction)
    {
        //Add listener to dictionary

        if (!Instance.Listeners.ContainsKey(NotificationName))
            Instance.Listeners.Add(NotificationName, new List<Listener>());

        //Add object to listener list for this notification
        Instance.Listeners[NotificationName].Add(new Listener(Sender, listenerAction));
    }

    // third action version
    public static void AddListener(Component Sender, EventEnum NotificationName, IEnumerator listenerAction)
    {
        //Add listener to dictionary

        if (!Instance.Listeners.ContainsKey(NotificationName))
            Instance.Listeners.Add(NotificationName, new List<Listener>());

        //Add object to listener list for this notification
        Instance.Listeners[NotificationName].Add(new Listener(Sender, listenerAction));
    }

    //------------------------------------------------
    //Function to remove a listener for a notification
    public static void RemoveListener(Component Sender, EventEnum NotificationName)
    {
        //If no key in dictionary exists, then exit
        if (!Instance.Listeners.ContainsKey(NotificationName))
            return;

        //Cycle through listeners and identify component, and then remove
        for (int i = Instance.Listeners[NotificationName].Count - 1; i >= 0; i--)
        {
            //Check instance ID
            if (Instance.Listeners[NotificationName][i].Component.GetInstanceID() == Sender.GetInstanceID())
                Instance.Listeners[NotificationName].RemoveAt(i); //Matched. Remove from list
        }
    }
    //------------------------------------------------
    //Function to post a notification to a listener
    public static void Post(Component Sender, EventEnum NotificationName, object options=null)
    {

        
        var eventName = Enum.GetName(typeof(EventEnum), NotificationName).ToUpper();

        var logInfo = eventName + " (" + Sender.name + ", o:" + options+")";

        //If no key in dictionary exists, then exit
        if (!Instance.Listeners.ContainsKey(NotificationName))
        {
            logInfo += "\nLISTENERS: NONE";
            //VoxelBusters.DebugPRO.Console.Log(eventName, logInfo);
            return;
        }

        //Else post notification to all matching listeners
        logInfo += "\nLISTENERS:";

        foreach (Listener listener in Instance.Listeners[NotificationName].ToArray())
        {

            logInfo += "\n- " + listener.Component.name + "";

            switch (listener.ActionType)
            {
                case 0:
                    listener.Action.Invoke(options);
                    logInfo += " (" + listener.Action.Method.Name+")";
                    break;
                case 1:
                    listener.ActionNoParams.Invoke();
                    logInfo += " (" + listener.ActionNoParams.Method.Name+")";
                    break;
                case 2:
                    Instance.StartCoroutine(listener.ActionEnum);
                    logInfo += " (" + listener.ActionEnum.ToString() + ")";
                    break;
            }
            //Debug.Log("Notify: "+listener.Component.name);
            
            //Listener.Sendm
            //Listener.SendMessage(NotificationName, Sender, SendMessageOptions.DontRequireReceiver);
        }


        Debug.Log(Enum.GetName(typeof(EventEnum), NotificationName) + " " + logInfo);
        //VoxelBusters.DebugPRO.Console.Log(Enum.GetName(typeof(EventEnum), NotificationName), logInfo);


    }


}