﻿using Assets;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventBehaviour : MonoBehaviour {
    

    public EventConfig[] ConfigList;

    public EventAction StartAction;

    void Awake()
    {

        foreach (var config in ConfigList)
        {
            config.Init(gameObject);
            Events.AddListener(this, config.Event, config.Invoke);
        }

        if (StartAction != EventAction.None)
        {
            var o = new EventConfig() { Action = StartAction };
            o.Init(gameObject);
            o.Invoke();
        }

    }

}


[Serializable]
public class EventConfig
{
    public EventEnum Event;
    public EventAction Action;
    private GameObject _go;

    public void Init(GameObject go)
    {
        _go = go;
    }
    
    public void Invoke()
    {
        switch (Action)
        {
            case EventAction.Hide:
                _go.SetActive(false);
                break;
            case EventAction.Show:
                _go.SetActive(true);
                break;
            case EventAction.Toggle:
                _go.SetActive(_go.activeSelf);
                break;
        }

    }
}

public enum EventAction
{
    None,
    Show,
    Hide,
    Toggle
}

