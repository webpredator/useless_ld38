﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class BuildingBehaviour : MonoBehaviour {

    public float MoveY = 0.1f;
    public float BuildDuration = 1;
    private BuildingConfig _config;

    void Start () {

        transform.localScale = new Vector3(1, 0, 1);
        transform.DOScaleY(1, BuildDuration);
        transform.DOMoveY(transform.position.y + MoveY, BuildDuration);

        
		
	}

    internal void SetConfig(BuildingConfig buildingConfig)
    {
        _config = buildingConfig;

        _spriteRenderer = GetComponent<SpriteRenderer>();
        _spriteRenderer.sprite = _config.Sprite;
        _spriteRenderer.color = _config.Color;
        _defaultColor = _spriteRenderer.color;
        _blinkColor = Color.white;

        _boxCollider = GetComponent<BoxCollider2D>();
        _boxCollider.enabled = false;

        var msgPos = transform.position + new Vector3(0, 0.025f, 0);
        var cost = "-";
        if (_config.CostWood > 0) cost += string.Format("{0:0}w ", _config.CostWood);
        if (_config.CostGold > 0) cost += string.Format("{0:0}g ", _config.CostGold);
        SpawnController.Instance.SpawnUiMsg(msgPos, cost.Trim(), Color.white);


        Events.Post(this, Assets.EventEnum.BuiltBuilding);


        //InvokeRepeating("PerSecondStuff", _config.ProduceInterval, _config.ProduceInterval);
        Invoke("SetReadyForHarvest", _config.ProduceInterval);

        InvokeRepeating("Blink", 0, 0.1f);


    }

    private void Blink()
    {
        if (!_readyForHarvest) return;
        _spriteRenderer.color = _spriteRenderer.color == _defaultColor ? _blinkColor : _defaultColor;
    }

    bool _readyForHarvest = false;
    private SpriteRenderer _spriteRenderer;
    private Color _defaultColor;
    private Color _blinkColor;
    private BoxCollider2D _boxCollider;

    void SetReadyForHarvest()
    {
        _readyForHarvest = true;
        _boxCollider.enabled = true;
    }

    public void Harvest()
    {
        if (!_readyForHarvest) return;
        _readyForHarvest = false;
        _boxCollider.enabled = false;

        _spriteRenderer.color = _defaultColor;
       // transform.localScale = Vector3.one;

        PerSecondStuff();

        Invoke("SetReadyForHarvest", _config.ProduceInterval);

    }


    /*
    private void OnMouseUp()
    {
        if (!_readyForHarvest) return;
        _readyForHarvest = false;

        _spriteRenderer.color = _defaultColor;
        transform.localScale = Vector3.one;

        PerSecondStuff();

        Invoke("SetReadyForHarvest", _config.ProduceInterval);

    }

    private void OnMouseEnter()
    {
        if (!_readyForHarvest) return;
        transform.localScale = Vector3.one * 1.4f;
    }

    private void OnMouseExit()
    {
        if (!_readyForHarvest) return;
        transform.localScale = Vector3.one;
    }
    */

    void PerSecondStuff() {

        var res = ResourceController.Instance;
        var spawner = SpawnController.Instance;

        var production = 10 + (Mathf.Clamp(res.People, 0, 100) / 10);
        var prodString = string.Format("{0:0.0}", production);

        // bounce a little
        transform.DOJump(transform.position, 0.003f, 1, 0.2f);
        var msgPos = transform.position + new Vector3(0, 0.025f, 0);

        switch (_config.SpawnType)
        {
            case SpawnTypeEnum.Person1:
            case SpawnTypeEnum.Person10:
                var amount = _config.SpawnType == SpawnTypeEnum.Person1 ? 1 : 10;
                SpawnController.Instance.SpawnPerson(transform.position, amount);
                res.People += amount;
                //spawner.SpawnUiMsg(transform.position, "1", Color.white);
                Events.Post(this, Assets.EventEnum.PersonCreated);
                break;
            case SpawnTypeEnum.Wood1:
                res.Wood+= production;
                spawner.SpawnUiMsg(msgPos, prodString + "w", Color.grey);
                Events.Post(this, Assets.EventEnum.GotWood);
                break;
            case SpawnTypeEnum.Gold1:
                res.Gold += production;
                spawner.SpawnUiMsg(msgPos, prodString + "g", Color.yellow);
                Events.Post(this, Assets.EventEnum.GotGold);
                break;
            case SpawnTypeEnum.Prayer1:
                res.Prayers += production;
                spawner.SpawnUiMsg(msgPos, prodString + "p", Color.blue);
                Events.Post(this, Assets.EventEnum.PrayerSent);
                break;
        }


    }

    

}
