﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimMasterController : MonoBehaviour {


    public static AnimMasterController Instance { get; private set; }
    private void Awake()
    {
        Instance = this;
        Events.AddListener(this, Assets.EventEnum.IntroStart, StartIntro());
        Events.AddListener(this, Assets.EventEnum.OutroStart, StartOutro());
    }

    public Animator[] AnimEye;
    public Animator[] AnimMouth;
    public Animator[] AnimArm;


    public AlienAnim[] Animations;

    

    IEnumerator StartIntro()
    {
        StartAnim(0);
        yield return new WaitForSeconds(Animations[0].Duration);
        Events.Post(this, Assets.EventEnum.IntroComplete);
    }

    IEnumerator StartOutro()
    {
        StartAnim(1);
        yield return new WaitForSeconds(Animations[1].Duration);
        Events.Post(this, Assets.EventEnum.OutroComplete);
    }

    public void StartAnim(int index)
    {
        var a = Animations[index];
        for (int i = 0; i < a.Anim.Length; i++)
        {
            var c = a.Anim[i];
            StartCoroutine(Exec(c.Time, index, i));
        }

    }

    IEnumerator Exec(float delay, int animIndex, int stepIndex)
    {
        yield return new WaitForSeconds(delay);
        var a = Animations[animIndex].Anim[stepIndex];
        Debug.Log("exec "+a.Action+" for "+a.Alien);
        switch (a.Action)
        {
            case AlienAnimAction.EyeDown: AnimEye[a.Alien].SetTrigger("down"); break;
            case AlienAnimAction.EyeUp: AnimEye[a.Alien].SetTrigger("up"); break;
            case AlienAnimAction.MouthIdle: AnimMouth[a.Alien].SetTrigger("idle"); break;
            case AlienAnimAction.MouthTalk:
                AnimMouth[a.Alien].SetTrigger("talk");
                Events.Post(this, Assets.EventEnum.Talking);
                break;
            case AlienAnimAction.MouthSurprised: AnimMouth[a.Alien].SetTrigger("surprised"); break;
            case AlienAnimAction.ArmIdle: AnimArm[a.Alien].SetTrigger("idle"); break;
            case AlienAnimAction.ArmUp: AnimArm[a.Alien].SetTrigger("up"); break;
            case AlienAnimAction.MouthLaugh:
                AnimMouth[a.Alien].SetTrigger("laugh");
                Events.Post(this, Assets.EventEnum.Laughing);
                break;
        }
    }
	

}

[Serializable]
public class AlienAnim
{
    public string Title;
    public float Duration = 10;
    public AlienAnimConfig[] Anim;
}

[Serializable]
public class AlienAnimConfig
{
    public int Alien;
    public float Time;    
    public AlienAnimAction Action;
}

public enum AlienAnimAction
{
    EyeDown,
    EyeUp,
    MouthIdle,
    MouthTalk,
    MouthSurprised,
    ArmIdle,
    ArmUp,
    MouthLaugh,
}