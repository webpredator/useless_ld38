﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlanetCrashBehaviour : MonoBehaviour {

    public GameObject LeftPlanet;
    public GameObject RightPlanet;
    public GameObject Explosion;

    public Vector3 LeftPlanetTarget;
    public Vector3 RightPlanetTarget;
    public float Speed;
    public float ExplosionDelay = 3;

    private void Awake()
    {
        Events.AddListener(this, Assets.EventEnum.OutroStart, OnOutroStart());
    }

    private void Start()
    {
       // StartCoroutine(OnOutroStart());
    }

    IEnumerator OnOutroStart() {

        LeftPlanet.SetActive(true);
        RightPlanet.SetActive(true);

        LeftPlanet.transform.DOMove(LeftPlanet.transform.position + LeftPlanetTarget, Speed);
        RightPlanet.transform.DOMove(RightPlanet.transform.position + RightPlanetTarget, Speed);

        yield return new WaitForSeconds(ExplosionDelay);
        Events.Post(this, Assets.EventEnum.Explosion);

        LeftPlanet.SetActive(false);
        RightPlanet.SetActive(false);
        Explosion.SetActive(true);

    }

}
