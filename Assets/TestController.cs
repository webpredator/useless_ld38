﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestController : MonoBehaviour {

    public GameObject PersonPrefab;
    public Vector3 PersonStart;
    public Collider2D PersonMovementBounds;
    public BuildMenuController BuildMenu;

    // Use this for initialization
    void Start () {

        

	}
	
	// Update is called once per frame
	public void CreatePerson (int amount) {

        for (int i = 0; i < amount; i++)
        {
            var go = (GameObject)Instantiate(PersonPrefab, PersonStart, Quaternion.identity);
            go.GetComponent<RandomMovement>().MovementBounds = PersonMovementBounds;

        }

    }

    public void CreateBuildings(int amount) {

        for (int i = 0; i < amount; i++)
        {
            var index = Random.Range(0, BuildMenu.BuildingConfigList.Length);
            //Debug.Log(index);
            BuildMenu.Build(index);
        }



    }


}
