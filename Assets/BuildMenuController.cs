﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BuildMenuController : MonoBehaviour {

    public GameObject Prefab;
    public BuildingConfig[] BuildingConfigList;

    public CircleCollider2D Planet;

    public static BuildMenuController Instance { get; private set; }
    void Awake() {
        Instance = this;
    }


    public void Build(int index) {
        Build(BuildingConfigList[index]);
    }


    public void Build(BuildingConfig config)
    {
        var pos = Planet.transform.position;
        var rand = (Random.insideUnitCircle * Planet.radius);
        pos.x += rand.x;
        pos.y += rand.y;

        var go = (GameObject)Instantiate(Prefab, pos, Quaternion.identity);
        go.GetComponent<BuildingBehaviour>().SetConfig(config);

    }

    void Start()
    {
        foreach(var b in BuildingConfigList)
        {
            if (b.BuildButton != null)
            {
                b.BuildButton.SetConfig(b);
            }
        }
    }

}

[Serializable]
public class BuildingConfig
{
    public string Title;
    public Sprite Sprite;
    public Color Color;
    public int CostWood;
    public int CostGold;
    public BuildButtonBehaviour BuildButton;
    public SpawnTypeEnum SpawnType;
    public float ProduceInterval;
}
